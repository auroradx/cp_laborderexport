Imports System.Data
Imports System.Data.SqlClient


Public Class frmMain
    Dim intInterval As Integer
    Dim intIntervalMax As Integer = 5
    Private ConnectString As String = Configuration.ConfigurationManager.ConnectionStrings("DBServer").ToString() ''"Provider=SQLOLEDB.1;Password=stranded;Persist Security Info=True;User ID=appusers;Initial Catalog=Cunningham;Data Source=CPASQL"
    Private strPath As String = Configuration.ConfigurationManager.AppSettings("ExportFileDirectory")  ''"\\vcpafile\Groups\InformationSystems\Interfaces\Bernhardt\Orders\"
    Private interfaceId As Integer = Configuration.ConfigurationManager.AppSettings("InterfaceID")

    Private Sub PollDatabase()
        Dim objADO As New CP_ADO2005.clsADO
        Dim dtOrders As DataTable
        Dim objOrders As clsHL7OrdersExport
        Dim strOrder As String
        Dim strFilename As String
        Dim intMSH_ID As Integer
        objADO.Connection_String = ConnectString
        objADO.ProviderType = CP_ADO2005.clsADO.eProvType.enuOLEDB
        objADO.PersistConnection()
        dtOrders = objADO.GetDataTable("Select MSH_ID from tblOrderMSH Where InterfaceID = " & interfaceId & " and Processed = 0 ")
        For I As Integer = 0 To dtOrders.Rows.Count - 1
            objOrders = New clsHL7OrdersExport
            objOrders.objADO = objADO
            If IsNumeric("" & dtOrders.Rows(I).Item(0)) Then
                intMSH_ID = Integer.Parse("" & dtOrders.Rows(I).Item(0))
                strOrder = objOrders.ConstructHL7(intMSH_ID)
                strFilename = "HL7_Order" & Now.ToString("yyyyMMddmmss") & "-" & intMSH_ID & ".hl7"
                If SaveFile(strPath, strFilename, strOrder) Then
                    'Update database 
                    objADO.Execute("Update tblOrderMSH Set Processed = 1 Where MSH_ID = " & intMSH_ID & " and InterfaceID = " & interfaceId)
                End If
            End If
        Next
        objADO.Disconnect()
        objADO = Nothing
    End Sub
    Private Function SaveFile(ByVal strPath As String, ByVal strFilename As String, ByVal strContents As String) As Boolean
        Try
            System.IO.File.WriteAllText(strPath & strFilename, strContents)
            Return True
        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        End Try
    End Function

    Private Sub btnPoll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPoll.Click
        PollDatabase()
    End Sub

    Private Sub tmrWait_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles tmrWait.Tick
        intInterval += 1
        If intInterval >= intIntervalMax Then
            PollDatabase()
            intInterval = 0
        End If
    End Sub

    Private Sub frmMain_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        tmrWait.Enabled = True
        tmrWait.Start()
    End Sub

    Private Sub btnRegen_Click(sender As Object, e As EventArgs) Handles btnRegen.Click

        ''check date range to make sure its valid
        If (dtStart.Value > dtEnd.Value) Then
            MessageBox.Show("Start date cannot be after end date.", "Check Date Range", MessageBoxButtons.OK)
        Else
            Dim sql As String = ""
            sql = "select * from tblOrderMSH msh inner join tblAccession a on msh.UniqAccessionId = a.UniqAccessionID "
            sql = sql & "where msh.InterfaceId = " & interfaceId & " And "
            sql = sql & "(a.InitialAccessionDate >= '" & dtStart.Value & "' and a.InitialAccessionDate < '" & DateAdd(DateInterval.Day, 1, dtEnd.Value) & "')"
            Dim objADO As New CP_ADO2005.clsADO
            Dim dtOrders As DataTable
            Dim objOrders As clsHL7OrdersExport
            Dim strOrder As String
            Dim strFilename As String
            Dim intMSH_ID As Integer
            objADO.Connection_String = ConnectString
            objADO.ProviderType = CP_ADO2005.clsADO.eProvType.enuOLEDB
            objADO.PersistConnection()
            dtOrders = objADO.GetDataTable("Select MSH_ID from tblOrderMSH Where InterfaceID = " + interfaceId)
            For I As Integer = 0 To dtOrders.Rows.Count - 1
                objOrders = New clsHL7OrdersExport
                objOrders.objADO = objADO
                If IsNumeric("" & dtOrders.Rows(I).Item(0)) Then
                    intMSH_ID = Integer.Parse("" & dtOrders.Rows(I).Item(0))
                    strOrder = objOrders.ConstructHL7(intMSH_ID)
                    strFilename = "HL7_Order" & Now.ToString("yyyyMMddmmss") & "-" & intMSH_ID & ".hl7"
                    If SaveFile(strPath, strFilename, strOrder) Then
                        'Update database 
                        objADO.Execute("Update tblOrderMSH Set Processed = 1 Where MSH_ID = " & intMSH_ID & " and interfaceId = " & interfaceId)
                    End If
                End If
            Next
            objADO.Disconnect()
            objADO = Nothing
            MessageBox.Show("All files have been regenerated.")
            grpRegen.Visible = False
        End If



    End Sub

    Private Sub RegenerateExportFiles()
        Dim objADO As New CP_ADO2005.clsADO
        Dim dtOrders As DataTable
        Dim objOrders As clsHL7OrdersExport
        Dim strOrder As String
        Dim strFilename As String
        Dim intMSH_ID As Integer
        objADO.Connection_String = ConnectString
        objADO.ProviderType = CP_ADO2005.clsADO.eProvType.enuOLEDB
        objADO.PersistConnection()
        dtOrders = objADO.GetDataTable("Select MSH_ID from tblOrderMSH Where InterfaceID = " + interfaceId)
        For I As Integer = 0 To dtOrders.Rows.Count - 1
            objOrders = New clsHL7OrdersExport
            objOrders.objADO = objADO
            If IsNumeric("" & dtOrders.Rows(I).Item(0)) Then
                intMSH_ID = Integer.Parse("" & dtOrders.Rows(I).Item(0))
                strOrder = objOrders.ConstructHL7(intMSH_ID)
                strFilename = "HL7_Order" & Now.ToString("yyyyMMddmmss") & "-" & intMSH_ID & ".hl7"
                If SaveFile(strPath, strFilename, strOrder) Then
                    'Update database 
                    objADO.Execute("Update tblOrderMSH Set Processed = 1 Where MSH_ID = " & intMSH_ID)
                End If
            End If
        Next
        objADO.Disconnect()
        objADO = Nothing
    End Sub

    Private Sub btnRegenerate_Click(sender As Object, e As EventArgs) Handles btnRegenerate.Click
        grpRegen.Visible = True
    End Sub
End Class
