<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnPoll = New System.Windows.Forms.Button()
        Me.tmrWait = New System.Windows.Forms.Timer(Me.components)
        Me.grpRegen = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dtEnd = New System.Windows.Forms.DateTimePicker()
        Me.dtStart = New System.Windows.Forms.DateTimePicker()
        Me.btnRegen = New System.Windows.Forms.Button()
        Me.btnRegenerate = New System.Windows.Forms.Button()
        Me.grpRegen.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnPoll
        '
        Me.btnPoll.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPoll.Location = New System.Drawing.Point(269, 12)
        Me.btnPoll.Name = "btnPoll"
        Me.btnPoll.Size = New System.Drawing.Size(112, 32)
        Me.btnPoll.TabIndex = 0
        Me.btnPoll.Text = "&Poll"
        Me.btnPoll.UseVisualStyleBackColor = True
        '
        'tmrWait
        '
        Me.tmrWait.Interval = 60000
        '
        'grpRegen
        '
        Me.grpRegen.Controls.Add(Me.Label2)
        Me.grpRegen.Controls.Add(Me.Label1)
        Me.grpRegen.Controls.Add(Me.dtEnd)
        Me.grpRegen.Controls.Add(Me.dtStart)
        Me.grpRegen.Controls.Add(Me.btnRegen)
        Me.grpRegen.Location = New System.Drawing.Point(12, 66)
        Me.grpRegen.Name = "grpRegen"
        Me.grpRegen.Size = New System.Drawing.Size(369, 112)
        Me.grpRegen.TabIndex = 1
        Me.grpRegen.TabStop = False
        Me.grpRegen.Text = "Regenerate Files"
        Me.grpRegen.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 54)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(55, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "End Date:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 28)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(58, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Start Date:"
        '
        'dtEnd
        '
        Me.dtEnd.Location = New System.Drawing.Point(80, 54)
        Me.dtEnd.Name = "dtEnd"
        Me.dtEnd.Size = New System.Drawing.Size(200, 20)
        Me.dtEnd.TabIndex = 2
        '
        'dtStart
        '
        Me.dtStart.Location = New System.Drawing.Point(80, 28)
        Me.dtStart.Name = "dtStart"
        Me.dtStart.Size = New System.Drawing.Size(200, 20)
        Me.dtStart.TabIndex = 1
        '
        'btnRegen
        '
        Me.btnRegen.Location = New System.Drawing.Point(288, 82)
        Me.btnRegen.Name = "btnRegen"
        Me.btnRegen.Size = New System.Drawing.Size(75, 23)
        Me.btnRegen.TabIndex = 0
        Me.btnRegen.Text = "Regenerate"
        Me.btnRegen.UseVisualStyleBackColor = True
        '
        'btnRegenerate
        '
        Me.btnRegenerate.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRegenerate.Location = New System.Drawing.Point(12, 12)
        Me.btnRegenerate.Name = "btnRegenerate"
        Me.btnRegenerate.Size = New System.Drawing.Size(112, 32)
        Me.btnRegenerate.TabIndex = 2
        Me.btnRegenerate.Text = "&Regenerate"
        Me.btnRegenerate.UseVisualStyleBackColor = True
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(393, 186)
        Me.Controls.Add(Me.btnRegenerate)
        Me.Controls.Add(Me.grpRegen)
        Me.Controls.Add(Me.btnPoll)
        Me.MaximizeBox = False
        Me.Name = "frmMain"
        Me.Text = "Bernhardt HL7 Orders Export"
        Me.grpRegen.ResumeLayout(False)
        Me.grpRegen.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnPoll As System.Windows.Forms.Button
    Friend WithEvents tmrWait As System.Windows.Forms.Timer
    Friend WithEvents grpRegen As GroupBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents dtEnd As DateTimePicker
    Friend WithEvents dtStart As DateTimePicker
    Friend WithEvents btnRegen As Button
    Friend WithEvents btnRegenerate As Button
End Class
