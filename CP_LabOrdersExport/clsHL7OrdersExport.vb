Public Class clsHL7OrdersExport
    Public objADO As CP_ADO2005.clsADO
    Public Shared strIdFieldReplace As String = "<<ID>>"
    Public Shared strElementSep As String = "|"
    Private strSQL_MSH As String = "Select EncodingCharacters, SendingApplication, SendingFacility, ReceivingApplication, ReceivingFacility, MessageDateTime, Security, MessageType, MessageControlID, ProcessingID, VersionID, SequenceNumber, ContinuationPointer, AcceptAcknowledgmentType, ApplicationAcknowledgeType, CountryCode, CharacterSet, MessagePrincipalLanguage, UniqAccessionId from tblOrderMSH where MSH_ID = " & strIdFieldReplace
    Private strSQL_PID As String = "SELECT SetIDPatientID, PatientIDExternal, PatientIDInternal, AlternatePatientID, PatientsName, MothersMaidenName, CASE WHEN CharIndex('/', DateOfBirth) > 0 THEN CONVERT(Varchar(26),CONVERT(DateTime, DateOfBirth), 112) ELSE DateOfBirth END AS DateOfBirth, Sex, PatientAlias, Race, PatientAddress, CountyCode, PhoneNumberHome, PhoneNumberBusiness, LanguagePatient, MaritalStatus, Religion, PatientAccountNumber, SSNPatient, DriversLicensePatient, MothersIdentifier, EthnicGroup, BirthPlace, MultipleBirthIndicator, BirthOrder, Citizenship, VeteransMilitaryStatus, Nationality, PatientDeathDateTime, PatientDeathIndicator FROM tblOrderPID WHERE (MSH_Id = " & strIdFieldReplace & ") Order By SetIDPatientID"
    Private strSQL_PV1 As String = "SELECT SetIdPV1, PatientClass, AssignedPatientLocation, AdmissionType, PreAdmitNumber, PriorPatientLocation, 'CP4078^Cunningham^physician' as AttendingDoctor, ReferringDoctor, ConsultingDoctor, HospitalService, TemporaryLocation, PreAdmitTestIndicator, ReAdmissionIndicator, AdmitSource, AmbulatoryStatus, VIPIndicators, AdmittingDoctor, PatientType, VisitNumber, FinancialClass, ChargePriceIndicator, CourtesyCode, CreditRating, ContractCode, ContractEffectiveDate, ContractAmount, ContractPeriod, InterestCode, TransferToBadDebtCode, TransferToBadDebtDate, BadDebtAgencyCode, BadDebtTransferAmount, BadDebtRecoveryAmount, DeleteAccountIndicator,DeleteAccountDate, DischargeDisposition, DischargedToLocation, DietType, ServicingFacility, BedStatus, AccountStatus, PendingLocation, PriorTemporaryLocation, " & _
                         "AdmitDateTime, DischargeDateTime, CurrentPatientBalance, TotalCharges, TotalAdjustments, TotalPayments, AlternateVisitID, VisitIndicator, OtherHealthCareProvider FROM tblOrderPV1 WHERE (MSH_ID = " & strIdFieldReplace & ") Order By SetIdPV1"
    Private strSQL_DG1 As String = "SELECT SetIDDiagnosis, DiagnosisCodingMethod, DiagnosisCode, DiagnosisDescription, DiagnosisDateTime, DiagnosisDRGType, MajorDiagnosticCategory, DiagnosticRelatedGroup, DRGApprovalIndicator, DRGGrouperReviewCode, OutlierType, OutlierDays, OutlierCost, GrouperVersionType, DiagnosisDRGPriority,DiagnosingClinician, DiagnosisSegmentification, ConfidentialIndicator, AttestationDateTime FROM tblOrderDG1 WHERE (MSH_ID = " & strIdFieldReplace & ") ORDER BY SetIDDiagnosis"
    Private strSQL_IN1 As String = "SELECT SetIDIN1, InsurancePlanID, InsuranceCompanyID, InsuranceCompanyName, InsuranceCompanyAddress, InsuranceCoContactPerson, InsuranceCoPhoneNumber, GroupNumber, GroupName, InsuredsGroupEmpID, InsuredsGroupEmpName, PlanEffectiveDate, PlanExpirationDate, AuthorizationInformation, PlanType, NameOfInsured, InsuredsRelationToPat, InsuredsDateOfBirth, InsuredsAddress, AssignmentOfBenefits, CoordinationOfBenefits, CoordOfBenPriority, NoticeOfAdmissionFlag, NoticeOfAdmissionDate, RptOfEligibilityFlag, RptOfEligibilityDate, ReleaseInformationCode, PreAdmitCertPAC, VerificationDateTime, " & _
                         "VerificationBy, TypeOfAgreementCode, BillingStatus, LifetimeReserveDays, DelayBeforeLRDay, CompanyPlanCode, PolicyNumber, PolicyDeductible, PolicyLimitAmount, PolicyLimitDays, RoomRateSemiPrivate, RoomRatePrivate, InsuredsEmployStatus, InsuredsSex, InsuredsEmployerAddr, VerificationStatus, PriorInsurancePlanID, CoverageType, Handicap, InsuredsIDNumber " & _
                         "FROM tblOrderIN1 WHERE (MSH_ID = " & strIdFieldReplace & ") ORDER BY SetIDIN1"

    Private strSQL_ORC As String = "SELECT OrderControl, PlacerOrderNumber, FillerOrderNumber, PlacerOrderNumber2, OrderStatus, ResponseFlag, QuantityTiming, Parent, DateTimeOfTransaction, EnteredBy, VerifiedBy, 'CP4078^Cunningham^physician' as OrderingProvider, EnterersLocation, CallBackPhoneNumber, OrderEffectiveDateTime, OrderControlCodeReason, EnteringOrganization, EnteringDevice, ActionBy FROM tblOrderORC WHERE (MSH_Id = " & strIdFieldReplace & ")"
    Private strSQL_OBR As String = "SELECT SetIDObservationRequest, PlacerOrderNumber, FillerOrderNumber, UniversalServiceID, Priority, RequestedDateTime, ObservationDateTime, ObservationEndDateTime, CollectionVolume, CollectorIdentifier, SpecimenActionCode, DangerCode, RelevantClinicalInfo, SpecimenRcvdDateTime, SpecimenSource, 'CP4078^Cunningham^physician' as OrderingProvider, OrderCallbackPhoneNumber, PlacersField1, PlacersField2, FillerField1, FillerField2, ResultsRptStatusChange, ChargeToPractice, " & _
                         "DiagnosticServiceSectID, ResultStatus, ParentResult, QuantityTiming, ResultCopiesTo, ParentNumber, TransportationMode, ReasonForStudy, PrincipalResultInterpreter, AssistantResultInterpreter, Technician, Transcriptionist, ScheduledDateTime, NumberOfSampleContainers, TransportLogisticsOfCollectedSample, CollectorsComment, TransportArrangementResponsibility, TransportArranged, EscortRequired, PlannedPatientTransportComment FROM tblOrderOBR WHERE (OBR_ID = " & strIdFieldReplace & ") ORDER BY SetIDObservationRequest"
    Private strSQL_OBX As String = "SELECT SetIdOBX, ValueType, ObservationIdentifier, ObservationSubId, ObservationResults, Units, ReferenceRange, AbnormalFlags, Probability, NatureOfAbnormalTest, ObservationResultStatus, DateLastNormalValue, UserDefinedAccessChecks, DateTimeOfObservation, ProducersID, ResponsibleObserver, ObservationMethod FROM tblOrderOBX WHERE (OBR_ID = " & strIdFieldReplace & ") ORDER BY SetIdOBX"

    Private strSQL_ReturnOBRID As String = "Select OBR_ID from tblOrderOBR Where PID_ID in (Select PID_ID from tblOrderPID Where (MSH_ID = " & strIdFieldReplace & "))"

    Public Function ConstructHL7(ByVal lngMSH As Integer) As String
        Dim strHL7 As String
        strHL7 = ReturnLineSegments("MSH", strSQL_MSH, lngMSH.ToString())
        strHL7 = strHL7 & ReturnLineSegments("PID", strSQL_PID, lngMSH.ToString())
        strHL7 = strHL7 & ReturnLineSegments("PV1", strSQL_PV1, lngMSH.ToString())
        strHL7 = strHL7 & ReturnLineSegments("DG1", strSQL_DG1, lngMSH.ToString())
        strHL7 = strHL7 & ReturnLineSegments("IN1", strSQL_IN1, lngMSH.ToString())
        strHL7 = strHL7 & ReturnLineSegments("ORC", strSQL_ORC, lngMSH.ToString())
        strHL7 = strHL7 & ReturnLineSegmentsOBR(lngMSH)
        Return Chr(11) & strHL7 & Chr(28) & Chr(13)
    End Function
    Private Function ReturnLineSegmentsOBR(ByVal lngMSH As Integer) As String
        Dim dtOBRs As DataTable = objADO.GetDataTable(strSQL_ReturnOBRID.Replace(strIdFieldReplace, lngMSH.ToString()))
        Dim strLine As String = ""
        Dim intOBR As Integer
        For I As Integer = 0 To dtOBRs.Rows.Count - 1
            intOBR = dtOBRs.Rows(I).Item(0)
            'OBR 16

            strLine = strLine & ReturnLineSegments("OBR", strSQL_OBR, "" & intOBR)
            strLine = strLine & ReturnLineSegments("OBX", strSQL_OBX, "" & intOBR)
        Next
        Return strLine
    End Function
    Private Function ReturnLineSegments(ByVal strSegmentIdentifier As String, ByVal strSQL As String, ByVal strFieldValue As String) As String
        Dim strReturn As String
        Dim objSB As New System.Text.StringBuilder()
        Dim dtTable As DataTable = objADO.GetDataTable(strSQL.Replace(strIdFieldReplace, strFieldValue))
        Dim strElement As String
        Dim intLastPopulatedColumn As Integer
        For I As Integer = 0 To dtTable.Rows.Count - 1
            intLastPopulatedColumn = -1
            For T As Integer = dtTable.Columns.Count - 1 To 0 Step -1
                If "" & dtTable.Rows(I).Item(T) <> "" Then
                    intLastPopulatedColumn = T
                    Exit For
                End If
            Next
            If intLastPopulatedColumn > -1 Then
                objSB.Append(strSegmentIdentifier & strElementSep)
                For T As Integer = 0 To intLastPopulatedColumn

                    strElement = dtTable.Rows(I).Item(T)
                    If strElement = "'" Then strElement = ""
                    objSB.Append(strElement)
                    If T < intLastPopulatedColumn Then
                        objSB.Append(strElementSep)
                    End If

                Next
                objSB.Append(Chr(13))
            End If
        Next
        strReturn = objSB.ToString()

        Return strReturn
    End Function

End Class
